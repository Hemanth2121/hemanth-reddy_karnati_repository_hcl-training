create database travel_KarnatiHemanthReddy;

use travel_KarnatiHemanthReddy;

create table PASSENGER
 (Passenger_name        varchar(20), 
  Category               varchar(20),
  Gender                varchar(20),
   Boarding_City         varchar(20),
   Destination_City      varchar(20),
  Distance               int,
  Bus_Type               varchar(20)
);


create table PRICE
(
             Bus_Type    varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;
select gender, count(*) from passenger where distance >= 600 group by gender;
select min(price) from price where Bus_Type = 'sleeper';
select Passenger_name from passenger where Passenger_name like 's%';
select Passenger_name, p1.Boarding_City, p1.Destination_city, p1.Bus_Type, p2.Price from passenger p1, price p2 where p1.Distance = p2.Distance and p1.Bus_type = p2.Bus_type group by p1.Passenger_name;
select p1.Passenger_name, p2.Price from passenger p1, price p2 where p1.Distance = 1000 and p1.Bus_type = 'Sitting';
select Price from passenger p1, price p2 where Passenger_name = 'Pallavi' and p1.Distance = p2.Distance ;
select distinct distance from passenger order by Distance desc;
select Passenger_name, Distance * 100.0/(select sum(Distance) from passenger) from passenger group by Distance;

create view p_view as select * from passenger where category = 'AC';
select * from p_view;
delimiter &&
	create procedure getpass()
	begin
	select * from passenger where Bus_Type = 'Sleeper';
	select count(Passenger_name) as totalpass from passenger
where Bus_Type = 'Sleeper';
	call getpass();
end &&
call getpass();
end &&
select * from passenger limit 5;
